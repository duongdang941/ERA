<?php

require_once 'LatakkoSync.php';

class LatakkoTruckAsync extends LatakkoSync
{

    protected $action = 'latakko_truck_update';
    protected $descExtra = "Transport: 48h";

    protected function insertProduct($data, $existing = null)
    {
        if ($existing == null) {
            $post_id = wp_insert_post([
                'post_title'   => "$data->brand $data->protector $data->width/$data->profile R$data->diameter",
                'post_content' => $data->description . "<br> $data->info <br>$this->descExtra",
                'post_status'  => 'publish',
                'post_type'    => "product",
            ]);
            wp_set_object_terms($post_id, 'simple', 'product_type');
        } else {
            $post_id = $existing->ID;
            if ( ! strpos($existing->post_content, $this->descExtra)) {
                $tyre = array(
                    'ID'           => $existing->ID,
                    'post_content' => $existing->post_content . " <br>$this->descExtra"
                );
                wp_update_post($tyre);
            }
        }

        $this->addCategories($post_id, $data);
        $this->addAttributes($post_id, $data);
        $this->updateMetas($post_id, $data);
        $this->updateStock($post_id, $data);

        $this->addImage($data->image, $post_id);

        error_log("Tyre $post_id updated");

        return $post_id; // if was success
    }

    protected function addCategories($post_id, $data)
    {
        if (strpos($data->aplication, "Buss")) {
            $this->addCategory($post_id, 'buss');
        }

        if (strpos($data->aplication, "Truck")) {
            $this->addCategory($post_id, 'veoauto');
        }

        if (strpos($data->road_for_trucks, "Winter")) {
            $this->addCategory($post_id, 'Talverehvid');
        }

        if (strpos($data->road_for_Buss, "Winter")) {
            $this->addCategory($post_id, 'Talverehvid');
        }

        $this->addCategory($post_id, 'Uued rehvid');
        $this->addCategory($post_id, 'Veoauto1');
    }

    protected function addAttributes($post_id, $data)
    {
        $truckTypes = $this->getTruckTypeAttributes($data);
        foreach ($truckTypes as $type) {
            $this->addAttribute($post_id, 'pa_soiduk', $type);
        }

        $axles = $this->getAxles($data);
        foreach ($axles as $axle) {
            $this->addAttribute($post_id, 'pa_sild', $axle);
        }

        $this->addAttribute($post_id, 'pa_laius', $data->width);
        $this->addAttribute($post_id, 'pa_korgus', $data->profile);
        $this->addAttribute($post_id, 'pa_diameeter', $data->diameter);
        $this->addAttribute($post_id, 'pa_tootja', $data->brand);
        $this->addAttribute($post_id, 'pa_seisukord', $this->getNewState('New'));

        $this->addAttribute($post_id, 'pa_noise', $data->noise);
        $this->addAttribute($post_id, 'pa_noise_grade', $data->noise_grade);
        $this->addAttribute($post_id, 'pa_wet', $data->wet);

        $this->addAttribute($post_id, 'pa_koormus-index', $this->getLoadIndex($data->li_si));
        $this->addAttribute($post_id, 'pa_kiirusindeks', $this->getSpeedIndex($data->li_si));
    }


}

