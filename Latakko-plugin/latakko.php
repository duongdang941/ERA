<?php
/**
 * Plugin Name: Latakko Product import
 * Plugin URI: https://wgentech.com/
 * Description: Imports products from Latakko system
 * Version: 2.0
 * Author: WGT JSC.
 * Author URI: https://wgentech.com/
 * Requires at least: 4.8.0
 * Tested up to: 4.9.8
 */



if ( ! defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

include_once 'LatakkoAsync.php';
require_once('LatakkoWoo.php');


wp_register_script('vue_js', plugins_url('js/vue.js', __FILE__));
wp_register_script('axios_js', plugins_url('js/axios.js', __FILE__));
wp_register_script('mini_toastr', plugins_url('js/mini-toastr.js', __FILE__));
wp_register_script('latakko_app_js', plugins_url('js/app.js', __FILE__), array(), time());
wp_register_style('latakko_app_css', plugins_url('css/main.css', __FILE__), array(), time());

wp_enqueue_script('vue_js');
wp_enqueue_script('axios_js', false, ['vue_js']);
wp_enqueue_script('mini_toastr', false, ['vue_js']);
wp_enqueue_script('latakko_app_js', false, ['vue_js', 'axios_js'], time(), true);
wp_enqueue_style('latakko_app_css', false, [], time(), true);

wp_localize_script("latakko_app_js",
    'latakko_settings', [
        'ajaxUrl' => admin_url('admin-ajax.php'),
    ]
);

//Get all Latakko products
add_action("wp_ajax_nopriv_latakko_get_product_data", "LatakkoWoo::getProductData");
add_action("wp_ajax_latakko_get_product_data", "LatakkoWoo::getProductData");
add_action("wp_ajax_nopriv_latakko_cancel_import_product", "LatakkoWoo::cancelImportProduct");
add_action("wp_ajax_latakko_cancel_import_product", "LatakkoWoo::cancelImportProduct");


add_action("wp_ajax_nopriv_latakko_continute_import_product", "LatakkoWoo::continuteImportProduct");
add_action("wp_ajax_latakko_continute_import_product", "LatakkoWoo::continuteImportProduct");

//Import Latakko products
add_action("wp_ajax_nopriv_latakko_import_product_data", "LatakkoWoo::importProductData");
add_action("wp_ajax_latakko_import_product_data", "LatakkoWoo::importProductData");

//get amount product entered
add_action("wp_ajax_nopriv_latakko_get_amount_product_entered", "LatakkoWoo::getAmountProductEntered");
add_action("wp_ajax_latakko_get_amount_product_entered", "LatakkoWoo::getAmountProductEntered");

add_action("wp_ajax_nopriv_latakko_setting_cron_sync_products", "LatakkoWoo::settingCronSyncProducts");
add_action("wp_ajax_latakko_setting_cron_sync_products", "LatakkoWoo::settingCronSyncProducts");


add_action('admin_init', 'LatakkoWoo::registerSettings');
add_action('admin_menu', 'LatakkoWoo::optionsPage');
add_action('init', 'LatakkoWoo::load_async_class');
add_action('syncProduct', 'LatakkoWoo::syncProduct');
add_action('startImport', 'LatakkoWoo::startImport');

// Create user token table
register_activation_hook( __FILE__, 'wp_create_product_data_import_table');
register_activation_hook( __FILE__, 'wp_create_queue_data_import_table');
register_deactivation_hook( __FILE__, 'delete_product_data_import_table' );
register_deactivation_hook( __FILE__, 'delete_queue_data_import_table' );
register_deactivation_hook (__FILE__, 'syncProductDeactivate');

function wp_create_product_data_import_table() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $table_name = $wpdb->prefix.'la_product_import';

    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE `$table_name` (
        id int(11) not null primary key auto_increment,
        product longtext NOT NULL,
        created_at datetime NOT NULL,
        expires_at datetime,
        UNIQUE KEY id (id)
    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function wp_create_queue_data_import_table() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $table_name = $wpdb->prefix.'la_queue_data_import';

    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE `$table_name` (
        id int(11) not null primary key auto_increment,
        option_name text NOT NULL,
        option_value longtext NOT NULL,
        created_at datetime NOT NULL,
        expires_at datetime,
        UNIQUE KEY id (id)
    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function delete_product_data_import_table() {
    global $wpdb;
    $table_name = $wpdb->prefix.'la_product_import';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
}
function delete_queue_data_import_table() {
    global $wpdb;
    $table_name = $table_name = $wpdb->prefix.'la_queue_data_import';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
}

function syncProductDeactivate() {
    $timestamp = wp_next_scheduled ('syncProduct');
    wp_unschedule_event ($timestamp, 'syncProduct');
}
