<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'reference' => '2b4b076f14c8a5eb5dfecb397d253c7d5529dfad',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'reference' => '2b4b076f14c8a5eb5dfecb397d253c7d5529dfad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'automattic/woocommerce' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'reference' => 'd3b292f04c0b3b21dced691ebad8be073a83b4ad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../automattic/woocommerce',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
