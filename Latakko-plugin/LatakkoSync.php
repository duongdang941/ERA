<?php
/**
 * Created by Margus Pala.
 * Date: 5.05.18
 * Time: 15:59
 */

require_once 'wp-async-request.php';
require_once 'wp-background-process.php';

abstract class LatakkoSync extends WP_Background_Process
{
    protected function getAxles($data)
    {
        $types = [];
        if (strpos($data->buss_possition, "All") || strpos($data->truck_possition, "All")) {
            $types[] = "[:et]abisild[:fi]apuakseli[:ru]ведомый[:]";
            $types[] = "[:et]esisild[:fi]etuakseli[:ru]передний[:]";
            $types[] = "[:et]haagisesild[:fi]perävaunuakseli[:ru]прицеп[:]";
            $types[] = "[:et]tagasild[:fi]takaakseli[:ru]задний[:]";
            $types[] = "[:et]veosild[:fi]vetoakseli[:ru]ведущий[:]";
        }

        if (strpos($data->buss_possition, "Drive") || strpos($data->truck_possition, "Drive")) {
            $types[] = "[:et]veosild[:fi]vetoakseli[:ru]ведущий[:]";
        }

        if (strpos($data->buss_possition, "Front") || strpos($data->truck_possition, "Front")) {
            $types[] = "[:et]esisild[:fi]etuakseli[:ru]передний[:]";
        }

        if (strpos($data->buss_possition, "Trailer") || strpos($data->truck_possition, "Trailer")) {
            $types[] = "[:et]haagis[:fi]perävaunu[:ru]прицеп[:]";
        }

        return $types;
    }

    protected function getTruckTypeAttributes($data)
    {
        $types = [];
        if (strpos($data->aplication, "Buss")) {
            $types[] = "[:et]buss[:fi]linja-auto[:ru]автобус[:]";
        }
        if (strpos($data->aplication, "Truck")) {
            $types[] = "[:et]veoauto[:fi]kuormaauto[:ru]грузовик[:]";
        }

        if (strpos($data->truck_possition, "Trailer")) {
            $types[] = "[:et]haagis[:fi]perävaunu[:ru]прицеп[:]";
        }

        if (strpos($data->buss_possition, "Trailer")) {
            $types[] = "[:et]haagis[:fi]perävaunu[:ru]прицеп[:]";
        }

        return $types;
    }

    protected function getSeason($data)
    {
        if ($data->season == "summer") {
            $hooaeg = "[:et]suverehv[:fi]kesärenkaat[:ru]лето[:]";
        } else if ($data->studdable == "studdable") {
            $hooaeg = "[:et]M+S[:fi]M+S[:ru]M+S[:]";
        } else {
            $hooaeg = "[:et]M+S[:fi]M+S[:ru]M+S[:]";
        }

        return $hooaeg;
    }

    protected function getNewState($input)
    {
        if ($input == 'New') {
            $state = "[:et]uus[:fi]uusi[:ru]новый[:]";
        } else {
            $state = "[:et]taastatud[:fi]taastatud[:ru]восст.[:]";
        }

        return $state;
    }

    protected function getLoadIndex($input)
    {
        $loadIndex = preg_replace('/[^0-9.]+/', '', $input);

        if (substr($loadIndex, 0, 1) == "1") {
            $loadIndex = substr($loadIndex, 0, 3);
        } else {
            $loadIndex = substr($loadIndex, 0, 2);
        }

        return $loadIndex;
    }

    protected function getSpeedIndex($input)
    {
        $input = trim($input);
        $index = "";
        if (strpos($input, "G")) {
            $index = "G - 90 km/h";
        } else if (strpos($input, "J")) {
            $index = "J - 100 km/h";
        } else if (strpos($input, "K")) {
            $index = "K - 110 km/h";
        } else if (strpos($input, "N")) {
            $index = "N - 120 km/h";
        } else if (strpos($input, "M")) {
            $index = "M - 130 km/h";
        } else if (strpos($input, "N")) {
            $index = "N - 140 km/h";
        } else if (strpos($input, "P")) {
            $index = "P - 150 km/h";
        } else if (strpos($input, "Q")) {
            $index = "Q - 160 km/h";
        } else if (strpos($input, "R")) {
            $index = "R - 170 km/h";
        } else if (strpos($input, "S")) {
            $index = "S - 180 km/h";
        } else if (strpos($input, "T")) {
            $index = "T - 190 km/h";
        } else if (strpos($input, "U")) {
            $index = "U - 200 km/h";
        } else if (strpos($input, "H")) {
            $index = "H - 210 km/h";
        } else if (strpos($input, "V")) {
            $index = "V - 240 km/h";
        } else if (strpos($input, "Z")) {
            $index = "Z - 240+ km/h";
        } else if (strpos($input, "W")) {
            $index = "W - 270 km/h";
        } else if (strpos($input, "Y")) {
            $index = "Y - 300 km/h";
        }

        return $index;
    }

    protected function getCarType($input)
    {
        if ($input == "car") {
            $soiduk = "sõiduauto";
        } else if ($input == "wan") {
            $soiduk = "kaubik";
        } else if ($input == "4x4") {
            $soiduk = "maastur";
        } else {
            $soiduk = "buss";
        }

        return $soiduk;
    }

    protected function getCarTypeAttribute($input)
    {
        if ($input == "car") {
            $soiduk = "[:et]sõiduauto[:fi]henkilöauto[:ru]легковой[:]";
        } else if ($input == "wan") {
            $soiduk = "[:et]kaubik[:fi]pakettiauto[:ru]микроавтобус[:]";
        } else if ($input == "truck") {
            $soiduk = "[:et]veoauto[:fi]kuormaauto[:ru]грузовик[:]";
        } else if ($input == "4x4") {
            $soiduk = "[:et]maastur[:fi]внедорожник[:ru]внедорожник[:]";
        } else {
            $soiduk = "[:et]buss[:fi]linja-auto[:ru]автобус[:]";
        }

        return $soiduk;
    }

    protected function addCategory($post_id, $cat_name)
    {
        $term_ids = [];
        $terms    = wp_get_object_terms($post_id, 'product_cat');
        if (count($terms) > 0) {
            foreach ($terms as $item) {
                $term_ids[] = $item->term_id;
            }
        }

        $category   = get_term_by('name', $cat_name, 'product_cat');
        $term_ids[] = $category->term_id;
        wp_set_object_terms($post_id, $term_ids, 'product_cat');
    }

    protected function addAttribute($product_id, $attribute_name, $attribute_value)
    {
        $data = array(
            $attribute_name => array(
                'name'         => $attribute_name,
                'value'        => '',
                'is_visible'   => '1',
                'is_variation' => '0',
                'is_taxonomy'  => '1'
            )
        );
        wp_set_object_terms($product_id, $attribute_value, $attribute_name, true);
        $_product_attributes = get_post_meta($product_id, '_product_attributes', true);
        if ($_product_attributes == null) {
            $_product_attributes = [];
        }
        update_post_meta($product_id, '_product_attributes', array_merge($_product_attributes, $data));
    }

    protected function task($item)
    {
        error_log("Updating Latakko item in background " . $item->stockcode);

        $existingProducts = get_posts(
            [
                'post_type'   => 'product',
                'meta_key'    => '_latakko_code',
                'meta_value'  => $item->stockcode,
                'post_status' => 'any'
            ]);

        if (count($existingProducts) == 0) {
            error_log('Inserting product');
            $this->insertProduct($item);
        } else {
            error_log('Updating stock: ' . $existingProducts[0]->ID);
            $this->insertProduct($item, $existingProducts[0]);
        }

        return false;
    }

    protected function addImage($imageId, $productId)
    {
        //Do not att another if attachment already there.
        if (has_post_thumbnail($productId)) {
            return;
        }

        $uploads = wp_upload_dir();

        $uploaded_image_path = $uploads['path'] . "/tyreimage_$imageId.jpg";
        $uploaded_image_url  = $uploads['url'] . "/tyreimage_$imageId.jpg";

        //If image already there then do not redownload.
        $attach_id = attachment_url_to_postid($uploaded_image_url);
        if ( ! $attach_id) {
            copy("http://i3.latakko.lv/images/tyres/$imageId.jpg", $uploaded_image_path);

            $wp_filetype = wp_check_filetype($uploaded_image_path, null);
            $attachment  = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => $imageId . ".jpg",
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attach_id = wp_insert_attachment($attachment, $uploaded_image_path, $productId);
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attach_data = wp_generate_attachment_metadata($attach_id, $uploaded_image_path);
            wp_update_attachment_metadata($attach_id, $attach_data);
        } else {
            error_log("Using previously downloaded image $attach_id $uploaded_image_url");
        }

        set_post_thumbnail($productId, $attach_id);
        add_post_meta($productId, '_product_image_gallery', $attach_id);
    }

    protected function updateMetas($post_id, $data)
    {
        update_post_meta($post_id, '_latakko_code', $data->stockcode);
        update_post_meta($post_id, '_visibility', 'visible');
        update_post_meta($post_id, '_downloadable', 'no');
        update_post_meta($post_id, '_regular_price', $data->baseprice);
        update_post_meta($post_id, '_featured', 'no');
        update_post_meta($post_id, '_weight', $data->weight_bruto);
        update_post_meta($post_id, '_sku', $data->ean);
        update_post_meta($post_id, '_price', $data->baseprice);
        update_post_meta($post_id, '_manage_stock', 'yes');
        update_post_meta($post_id, '_backorders', 'no');
    }

    protected function updateStock($post_id, $data)
    {
        update_post_meta($post_id, '_stock_status', intval($data->qty_available) == 0 ? 'outofstock' : 'instock');
        wc_update_product_stock($post_id, $data->qty_available);
    }


}