<?php

require __DIR__ . '/vendor/autoload.php';
include_once 'wp-background-process.php';
include_once 'LatakkoWoo.php';

use Automattic\WooCommerce\Client;

class ImportProductProcess extends WP_Background_Process
{
    const TOTAL_PRODUCT_SUCCEED = 'latakko_amount_product_entered';
    const TOTAL_PRODUCT_FAILED = 'latakko_amount_product_fail';
    const TOTAL_PRODUCT_NEED_IMPORT = 'latakko_total_product_import';
    const IMPORT_PRODUCT_STATUS = 'latakko_import_product_status';
    const IMPORT_PRODUCT_NAME = 'wgt_product_name_importing';

    const STATUS_PENDING  = 'pending';
    const STATUS_IN_PROCESS  = 'In Process';
    const STATUS_FAIL  = 'fail';
    const STATUS_CANCEL  = 'cancel';

    const STATUS_COMPLETE  = 'completed';

    protected $action = 'latakko_import_product';

    /**
     * @param $productList
     * @return bool
     */
    public function task($productList)
    {
        $latakkoWoo = new LatakkoWoo();
        $productList = $latakkoWoo->getProductList();
        $productList = array_slice($productList, get_option('start_number'),20);

        if ($this->isCancelImport()) {
            return true;
        }

        update_option(self::IMPORT_PRODUCT_STATUS, self::STATUS_IN_PROCESS);
        foreach ($productList as $product) {
            if ($this->isCancelImport()) {
                break;
            }
            update_option('start_number', get_option('start_number') + 1);
            try {
                update_option('wgt_product_name_importing', $product->ArticleText);
                $latakkoWoo->saveAttributeOfProduct($product);
                $category = $latakkoWoo->addCategory($product) ?? null;
                $latakkoWoo->importProduct($product, $category);
                update_option(self::TOTAL_PRODUCT_SUCCEED, get_option(self::TOTAL_PRODUCT_SUCCEED) + 1);
            } catch (Exception $exception) {
                update_option(self::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_FAIL);
                $productFail = get_option(self::TOTAL_PRODUCT_FAILED);
                update_option(self::TOTAL_PRODUCT_FAILED, $productFail + 1);
                error_log('Product: ' . $product->ArticleText . '. Error: '. $exception->getMessage());
                continue;
            }
        }

        if(get_option('start_number') < (get_option(self::TOTAL_PRODUCT_NEED_IMPORT))) {
            $latakkoWoo = new LatakkoWoo();
            $latakkoWoo->cronImport();
        } else {
            update_option(self::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_COMPLETE);
        }

        return false;
    }

    /**
     * @return void
     */
    protected function complete()
    {
        $totalSuccess = get_option(self::TOTAL_PRODUCT_SUCCEED);
        $totalFail = get_option(self::TOTAL_PRODUCT_FAILED);
        $totalProductNeedImport = get_option(self::TOTAL_PRODUCT_NEED_IMPORT);
        if (($totalSuccess + $totalFail) == (int)$totalProductNeedImport) {
            update_option(self::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_COMPLETE);
        } elseif ($this->isCancelImport()) {
            update_option(self::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_COMPLETE);
            update_option(self::TOTAL_PRODUCT_SUCCEED, 0);
            update_option(self::TOTAL_PRODUCT_FAILED, 0);
            update_option(self::TOTAL_PRODUCT_NEED_IMPORT, 0);
        }

        parent::complete();
    }

    /**
     * @return bool
     */
    public function isCancelImport()
    {
        return get_option('wgt_cancel_import') == 'true';
    }
}