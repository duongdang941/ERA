<?php

include_once 'wp-async-request.php';
include_once 'wp-background-process.php';

class LatakkoDeleteAsync extends WP_Background_Process
{
	protected $action = 'latakko_delete_expired';

	protected function task($post_id)
	{
		$validProducts = json_decode(get_option("latakko_valid_products"));
		$latakkoCode   = get_post_meta($post_id, $key = '_latakko_code', true);

		error_log("Checking if delete is needed post=$post_id, latakko=$latakkoCode");

		if ($latakkoCode != null && ! in_array($latakkoCode, $validProducts)) {
			error_log("Deleting post $post_id because its latakko code is not valid $latakkoCode");
			wp_delete_post($post_id);
		}

		return false;
	}
}