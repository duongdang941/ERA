<?php

require_once 'LatakkoSync.php';

class LatakkoAsync extends LatakkoSync
{
    protected $action = 'latakko_update';
    protected $descExtra = "Transport: 48h";

    protected function insertProduct($data, $existing = null)
    {
        if ($existing == null) {
            $post_id = wp_insert_post([
                'post_title'   => "$data->brand $data->protector $data->width/$data->profile R$data->diameter",
                'post_content' => $data->description . "<br> $data->info <br>$this->descExtra",
                'post_status'  => 'publish',
                'post_type'    => "product",
            ]);
            wp_set_object_terms($post_id, 'simple', 'product_type');
        } else {
            $post_id = $existing->ID;
            if ( ! strpos($existing->post_content, $this->descExtra)) {
                $tyre = array(
                    'ID'           => $existing->ID,
                    'post_content' => $existing->post_content . " <br>$this->descExtra"
                );
                wp_update_post($tyre);
            }
        }

        $this->addCategories($post_id, $data);
        $this->addAttributes($post_id, $data);
        $this->updateMetas($post_id, $data);
        $this->updateStock($post_id, $data);

        $this->addImage($data->image, $post_id);

        error_log("Tyre $post_id updated");

        return $post_id; // if was success
    }

    protected function addCategories($post_id, $data)
    {
        if ($data->season == "summer") {
            $this->addCategory($post_id, 'Suverehvid');
        } else if ($data->studdable == "studdable") {
            $this->addCategory($post_id, 'Talverehvid');
            $this->addCategory($post_id, 'Naelrehvid');
        } else {
            $this->addCategory($post_id, 'Talverehvid');
            $this->addCategory($post_id, 'Lamell rehvid');
        }

        if ($data->retread == "New") {
            $this->addCategory($post_id, 'Uued rehvid');
        }

        $this->addCategory($post_id, 'Sõiduauto1');
    }

    protected function addAttributes($post_id, $data)
    {
        $this->addAttribute($post_id, 'pa_soiduk', $this->getCarTypeAttribute($data->type));
        $this->addAttribute($post_id, 'pa_hooaeg', $this->getSeason($data));
        $this->addAttribute($post_id, 'pa_laius', $data->width);
        $this->addAttribute($post_id, 'pa_korgus', $data->profile);
        $this->addAttribute($post_id, 'pa_diameeter', $data->diameter);
        $this->addAttribute($post_id, 'pa_tootja', $data->brand);
        $this->addAttribute($post_id, 'pa_seisukord', $this->getNewState($data->retread));

        $this->addAttribute($post_id, 'pa_noise', $data->noise);
        $this->addAttribute($post_id, 'pa_noise_grade', $data->noise_grade);
        $this->addAttribute($post_id, 'pa_rolling_resistance', $data->rrc);
        $this->addAttribute($post_id, 'pa_wet', $data->wet);

        $this->addAttribute($post_id, 'pa_koormus-index', $this->getLoadIndex($data->li));
        $this->addAttribute($post_id, 'pa_kiirusindeks', $this->getSpeedIndex($data->si));
    }

}

