if (document.getElementById('latakko_app')) {

    miniToastr.init({
        appendTarget: document.body
    });

    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    let getTimeOfImportInterval;
    const setVisible = (elementOrSelector, visible) =>
        (typeof elementOrSelector === 'string'
                ? document.querySelector(elementOrSelector)
                : elementOrSelector
        ).style.display = visible ? 'block' : 'none';

    let vm = new Vue({
        el: "#latakko_app",
        methods: {
            showOptionCron: function() {
                document.getElementById('option_cron').style.display = 'revert'
            },

            settingCron: function() {
                var productsSyncEnabled = document.getElementById('products_sync_enabled').value ?? 'false';
                var cronStartTime = document.getElementById('latakko_cron_start_time').value ?? null;
                var cronFrequency = document.getElementById('latakko_cron_frequency').value ?? 'daily';
                document.getElementById('option_cron').style.display = 'none'
                axios.get(latakko_settings.ajaxUrl, {
                    params: {
                        action: "latakko_setting_cron_sync_products",
                        productsSyncEnabled: productsSyncEnabled,
                        cronStartTime: cronStartTime,
                        cronFrequency: cronFrequency
                    }
                }).then(function (response) {
                    if (response.data.status == "OK") {
                        miniToastr.success(response.data.message);
                    }
                }).catch(function (response) {
                    miniToastr.success(response.data.message);
                });
            },

            showPopup: function (event) {
                document.getElementById('popup').style.display = 'revert'
            },
            cancelImport: function (event) {
                setVisible('#loading', true);

                axios.get(latakko_settings.ajaxUrl, {
                    params: {
                        action: "latakko_cancel_import_product"
                    }
                }).then(function (response) {
                    window.location.reload();
                    setVisible('#loading', false);
                    //todo handle response.
                }).catch(function (response) {
                    window.location.reload();
                    setVisible('#loading', false);
                    //todo handle response.
                });
            },

            continuteImport: function (event) {
                setVisible('#loading', true);

                axios.get(latakko_settings.ajaxUrl, {
                    params: {
                        action: "latakko_continute_import_product"
                    }
                }).then(function (response) {
                    window.location.reload();
                    setVisible('#loading', false);
                    //todo handle response.
                }).catch(function (response) {
                    window.location.reload();
                    setVisible('#loading', false);
                    //todo handle response.
                });
            },

            getProductData: function (event) {
                var latakkoApiUser = document.getElementById('latakko_api_user').value ?? "";
                var latakkoApiPassword = document.getElementById('latakko_api_pass').value ?? "";
                var wgtMessageProcess = document.getElementById('wgt-message-processing');
                var miniToastrNotificationMessage = document.getElementById('mini-toastr-notification__message');
                document.getElementById('get_product_data').setAttribute('disabled', 'disabled')
                if (!latakkoApiUser) {
                    miniToastr.error('API UserName is invalid');
                } else if (!latakkoApiPassword) {
                    miniToastr.error('Rest Api is invalid');
                } else {
                    wgtMessageProcess.style.display = "block";
                    miniToastrNotificationMessage.innerHTML = 'Getting products from Api...';
                    axios.get(latakko_settings.ajaxUrl, {
                        params: {
                            action: "latakko_get_product_data"
                        }
                    }).then(function (response) {
                        if (response.data.status == "OK") {
                            wgtMessageProcess.style.display = 'none';
                            miniToastr.success(response.data.message);
                            vm.processingImport();

                        } else {
                            miniToastr.error(response.data.message);
                            document.getElementById('get_product_data').removeAttribute('disabled')
                            wgtMessageProcess.style.display = 'none';
                            window.location.reload();
                        }
                    }).catch(function (response) {
                        wgtMessageProcess.style.display = "none";
                        document.getElementById('get_product_data').removeAttribute('disabled')
                        window.location.reload();
                        console.log(response);
                        miniToastr.error(response);
                    });
                }
            },

            processingImport: function () {
                var wgtMessageProcess = document.getElementById('wgt-message-processing');
                var miniToastrNotificationMessage = document.getElementById('mini-toastr-notification__message');

                miniToastrNotificationMessage.innerHTML = 'Importing product...';
                wgtMessageProcess.style.display = 'block';
                axios.get(latakko_settings.ajaxUrl, {
                    params: {
                        action: "latakko_import_product_data"
                    }
                }).then(function (response) {
                    wgtMessageProcess.style.display = 'none';
                    if (response.data.status == "OK") {
                        miniToastr.success(response.data.message);
                        window.location.reload();
                        getTimeOfImportInterval = setInterval(vm.getTimeOfImport, 5000);
                    } else {
                        miniToastr.error(response.data.message);
                    }
                }).catch(function (response) {
                    miniToastr.error(response.data.message);
                    document.getElementById('import-product-status').innerHTML = 'fail'
                });
            },

            getTimeOfImport: function () {
                axios.get(latakko_settings.ajaxUrl, {
                    params: {
                        action: "latakko_get_amount_product_entered"
                    }
                }).then(function (response) {
                    if (response.data.status == "OK") {
                        var importProductStatus = response.data.importProductStatus;
                        var amountProductEntered = response.data.amountProductEntered;
                        var amountProductFail = response.data.amountProductFail,
                            productName = response.data.productName;
                        importProductStatus = importProductStatus ? importProductStatus : '';
                        amountProductEntered = amountProductEntered ? amountProductEntered : '0';
                        amountProductFail = amountProductFail ? amountProductFail : '0';
                        document.getElementById('amount-product-entered').innerHTML = amountProductEntered;
                        document.getElementById('amount-product-fail').innerHTML = amountProductFail;
                        if (productName != null) {
                            document.getElementById('wgt-product-name').innerHTML = 'Importing: '+ productName;
                        }
                        if(importProductStatus == 'completed') {
                            document.getElementById('import-product-status').innerHTML = importProductStatus;
                            clearInterval(getTimeOfImportInterval);
                        } else if (importProductStatus != 'pending') {
                            document.getElementById('import-product-status').innerHTML = importProductStatus;
                        } else if (importProductStatus == 'fail') {
                            clearInterval(getTimeOfImportInterval);
                        }
                    } else {
                        wgtMessageProcess.style.display = 'none';
                        miniToastr.error(response.data.message);
                    }
                })
            },
        }
    });

    window.onload = function () {
        var importProductStatusValue = document.getElementById('import-product-status-value'),
            importProductTotalValue = document.getElementById('import-product-total-value');
        if(importProductStatusValue != null && importProductTotalValue != null) {
            if (importProductStatusValue.value == 'completed') {
                document.getElementById('product-import-process').style.display = "none"
            } else if (importProductStatusValue.value == 'pending' || importProductStatusValue.value == 'In Process') {
                getTimeOfImportInterval = setInterval(vm.getTimeOfImport, 5000);
            }
            vm.getTimeOfImport();
        }
    }
}

