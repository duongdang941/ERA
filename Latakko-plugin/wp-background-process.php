<?php
/**
 * WP Background Process
 *
 * @package WP-Background-Processing
 */

if ( ! class_exists('WP_Background_Process')) {

    /**
     * Abstract WP_Background_Process class.
     *
     * @abstract
     * @extends WP_Async_Request
     */
    abstract class WP_Background_Process extends WP_Async_Request
    {

        /**
         * Action
         *
         * (default value: 'background_process')
         *
         * @var string
         * @access protected
         */
        protected $action = 'background_process';

        /**
         * Start time of current process.
         *
         * (default value: 0)
         *
         * @var int
         * @access protected
         */
        protected $start_time = 0;

        /**
         * Cron_hook_identifier
         *
         * @var mixed
         * @access protected
         */
        protected $cron_hook_identifier;

        /**
         * Cron_interval_identifier
         *
         * @var mixed
         * @access protected
         */
        protected $cron_interval_identifier;

        /**
         * Initiate new background process
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Dispatch
         *
         * @access public
         * @return void
         */
        public function dispatch()
        {
            // Perform remote post.
            return parent::dispatch();
        }

        /**
         * Push to queue
         *
         * @param mixed $data Data.
         *
         * @return $this
         */
        public function push_to_queue($data)
        {
            $this->data[] = $data;

            return $this;
        }

        /**
         * Save queue
         *
         * @return $this
         */
        public function save()
        {
            $key = $this->generate_key();

            if ( ! empty($this->data)) {
                $this->saveQueueDataImport($key, $this->data);
            }

            return $this;
        }

        /**
         * Update queue
         *
         * @param string $key Key.
         * @param array $data Data.
         *
         * @return $this
         */
        public function update($key, $data)
        {
            if ( ! empty($data)) {
                update_option($key, $data);
            }

            return $this;
        }

        /**
         * Delete queue
         *
         * @param string $key Key.
         *
         * @return $this
         */
        public function delete($key)
        {
            delete_site_option($key);

            return $this;
        }

        /**
         * Generate key
         *
         * Generates a unique key based on microtime. Queue items are
         * given a unique key so that they can be merged upon save.
         *
         * @param int $length Length.
         *
         * @return string
         */
        protected function generate_key($length = 64)
        {
            $unique  = md5(microtime() . rand());
            $prepend = $this->identifier . '_batch_';

            return substr($prepend . $unique, 0, $length);
        }

        /**
         * Maybe process queue
         *
         * Checks whether data exists within the queue and that
         * the process is not already running.
         */
        public function maybe_handle()
        {
            // Don't lock up other requests while processing
            session_write_close();

            if ($this->is_process_running()) {
                // Background process already running.
                wp_die();
            }

            if ($this->isQueueEmpty()) {
                // No data to process.
                wp_die();
            }

            check_ajax_referer($this->identifier, 'nonce');

            $this->handle();

            wp_die();
        }

        /**
         * Is queue empty
         *
         * @return bool
         */
        protected function is_queue_empty()
        {
            global $wpdb;

            $table  = $wpdb->options;
            $column = 'option_name';

            if (is_multisite()) {
                $table  = $wpdb->sitemeta;
                $column = 'meta_key';
            }

            $key = $wpdb->esc_like($this->identifier . '_batch_') . '%';

            $count = $wpdb->get_var($wpdb->prepare("
			SELECT COUNT(*)
			FROM {$table}
			WHERE {$column} LIKE %s
		", $key));

            return ($count > 0) ? false : true;
        }

        /**
         * Is process running
         *
         * Check whether the current process is already running
         * in a background process.
         */
        public function is_process_running()
        {
            if (get_site_transient($this->identifier . '_process_lock')) {
                // Process already running.
                return true;
            }

            return false;
        }

        /**
         * Lock process
         *
         * Lock the process so that multiple instances can't run simultaneously.
         * Override if applicable, but the duration should be greater than that
         * defined in the time_exceeded() method.
         */
        protected function lock_process()
        {
            $this->start_time = time(); // Set start time of current process.

            $lock_duration = (property_exists($this, 'queue_lock_time')) ? $this->queue_lock_time : 1400; // 1 minute
            $lock_duration = apply_filters($this->identifier . '_queue_lock_time', $lock_duration);

            set_site_transient($this->identifier . '_process_lock', microtime(), $lock_duration);
        }

        /**
         * Unlock process
         *
         * Unlock the process so that other instances can spawn.
         *
         * @return $this
         */
        protected function unlock_process()
        {
            delete_site_transient($this->identifier . '_process_lock');

            return $this;
        }

        /**
         * Get batch
         *
         * @return stdClass Return the first batch from the queue
         */
        protected function get_batch()
        {
            global $wpdb;

            $table        = $wpdb->options;
            $column       = 'option_name';
            $key_column   = 'option_id';
            $value_column = 'option_value';

            if (is_multisite()) {
                $table        = $wpdb->sitemeta;
                $column       = 'meta_key';
                $key_column   = 'meta_id';
                $value_column = 'meta_value';
            }

            $key = $wpdb->esc_like($this->identifier . '_batch_') . '%';

            $query = $wpdb->get_row($wpdb->prepare("
			SELECT *
			FROM {$table}
			WHERE {$column} LIKE %s
			ORDER BY {$key_column} ASC
			LIMIT 1
		", $key));

            $batch       = new stdClass();
            $batch->key  = $query->$column;
            $batch->data = maybe_unserialize($query->$value_column);

            return $batch;
        }

        /**
         * Handle
         *
         * Pass each queue item to the task handler, while remaining
         * within server memory and time limit constraints.
         */
        protected function handle()
        {
            $this->lock_process();

            $batch = $this->getBatch();
            do {
                foreach ($batch->data as $key => $value) {
                    $task = $this->task($value);
                    if (false !== $task) {
                        $batch->data[$key] = $task;
                    } else {
                        unset($batch->data[$key]);
                    }

                    if ($this->time_exceeded() || $this->memory_exceeded()) {
                        // Batch limits reached.
                        break;
                    }
                }

                // Update or delete current batch.
                if ( ! empty($batch->data)) {
                    $this->updateQueue($batch->key, $batch->data);
                } else {
                    $this->deleteQueue($batch->key);
                    $this->unlock_process();
                }
            } while ( ! $this->time_exceeded() && ! $this->memory_exceeded() && ! $this->isQueueEmpty());

            // Start next batch or complete process.
            if ( ! $this->isQueueEmpty()) {
                $this->dispatch();
            } else {
                $this->complete();
            }

            wp_die();
        }

        /**
         * Memory exceeded
         *
         * Ensures the batch process never exceeds 90%
         * of the maximum WordPress memory.
         *
         * @return bool
         */
        protected function memory_exceeded()
        {
            $memory_limit   = $this->get_memory_limit() * 0.9; // 90% of max memory
            $current_memory = memory_get_usage(true);
            $return         = false;

            if ($current_memory >= $memory_limit) {
                $return = true;
            }

            return apply_filters($this->identifier . '_memory_exceeded', $return);
        }

        /**
         * Get memory limit
         *
         * @return int
         */
        protected function get_memory_limit()
        {
            if (function_exists('ini_get')) {
                $memory_limit = ini_get('memory_limit');
            } else {
                // Sensible default.
                $memory_limit = '128M';
            }

            if ( ! $memory_limit || -1 === intval($memory_limit)) {
                // Unlimited, set to 32GB.
                $memory_limit = '32000M';
            }

            return intval($memory_limit) * 1024 * 1024;
        }

        /**
         * Time exceeded.
         *
         * Ensures the batch never exceeds a sensible time limit.
         * A timeout limit of 30s is common on shared hosting.
         *
         * @return bool
         */
        protected function time_exceeded()
        {
            $finish = $this->start_time + apply_filters($this->identifier . '_default_time_limit', 60); // 20 seconds
            $return = false;

            if (time() >= $finish) {
                $return = true;
            }

            return apply_filters($this->identifier . '_time_exceeded', $return);
        }

        /**
         * @param $key
         * @param $data
         * @return void
         */
        public function saveQueueDataImport($key, $data) {
            global $wpdb;

            $createAt = current_time( 'mysql' );
            $table_name = $wpdb->prefix.'la_queue_data_import';
            $format = array('%s','%s','%s','%s','%');
            $wpdb->insert(
                $table_name,
                array(
                    'option_name' => $key,
                    'option_value' => maybe_serialize($data),
                    'created_at' => $createAt
                ),
                $format
            );
        }

        /**
         * @return stdClass
         */
        public function getBatch () {
            global $wpdb;

            $table = $wpdb->prefix.'la_queue_data_import';

            $key = $wpdb->esc_like($this->identifier . '_batch_') . '%';

            $query =  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE option_name LIKE %s LIMIT 1", $key ));

            if(!empty($query) && !empty($query[0])) {
                $batch       = new stdClass();
                $batch->key  = $query[0]->option_name;
                $batch->data = maybe_unserialize($query[0]->option_value);
            }

            return $batch;
        }

        /**
         * @return bool
         */
        public function isQueueEmpty() {
            global $wpdb;

            $table = $wpdb->prefix.'la_queue_data_import';

            $key = $wpdb->esc_like($this->identifier . '_batch_') . '%';

            $query =  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE option_name LIKE %s", $key ));

            return (sizeof($query) > 0) ? false : true;
        }

        public function deleteQueue($optionName) {
            global $wpdb;
            $table  = $wpdb->prefix.'la_queue_data_import';

            $wpdb->query( $wpdb->prepare("DELETE FROM $table WHERE option_name = '$optionName'"));
        }

        /**
         * @param $optionName
         * @param $data
         * @return void
         */
        public function updateQueue($optionName, $data) {
            global $wpdb;
            $table  = $wpdb->prefix.'la_queue_data_import';

            $wpdb->query($wpdb->prepare("UPDATE $table SET option_value = '$data' WHERE option_name = '$optionName'"));
        }

        /**
         * @return void
         */
        public function cancelProcess () {
            global $wpdb;
            $table  = $wpdb->prefix.'la_queue_data_import';
            $wpdb->query(  $wpdb->prepare("DELETE FROM $table"));
        }

        /**
         * Task
         *
         * Override this method to perform any actions required on each
         * queue item. Return the modified item for further processing
         * in the next pass through. Or, return false to remove the
         * item from the queue.
         *
         * @param mixed $item Queue item to iterate over.
         *
         * @return mixed
         */
        abstract protected function task($item);

    }
}
