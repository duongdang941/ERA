<?php
require __DIR__ . '/vendor/autoload.php';
include_once 'LatakkoTruckAsync.php';
include_once 'ImportProductProcess.php';
include_once 'wp-async-request.php';
include_once 'wp-background-process.php';
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
require_once(ABSPATH . 'wp-load.php');
const GET_PRODUCT_API = 'https://api.latakko.eu/api/Articles';
const GET_IMAGE_API = 'https://api.latakko.eu/api/ArticleImages/';
const GET_TOKEN_API = 'api.latakko.eu/Token';
const CONTENT_TYPE = 'Content-Type: application/x-www-form-urlencoded';

const VERSION = ['version' => 'wc/v3'];
const TYPE_PRODUCT_DEFAULT = 'simple';
const STOCK_STATUS_DEFAULT = 'instock';
const MANAGE_STOCK_DEFAULT = 'yes';
const BACK_ORDER_DEFAULT = 'yes';
class LatakkoWoo
{
    protected $attributeCreationList = ['EAN', 'ManufacturerArticleId', 'BrandName', 'PatternModelText', 'PositionText',
        'Color', 'Extra', 'AspectRatio', 'Diameter', 'TreadDepth', 'Radial', 'ExtraLoad', 'RunFlat', 'LoadIndex',
        'SpeedIndex', 'FuelEffiency', 'WetGrip', 'NoiceClass', 'NoiceValue', 'TyreClass', 'FrictionRegion', 'SnowGrip',
        'IceGrip', 'EPRELId', 'NumberOfBolts', 'BoltCircle', 'SeatType', 'IsWinterApproved', 'MaximumLoad',
        'RecyclingFee', 'ExternalDeliveryTime', 'ExternalStockRegion', 'NetPrice', 'RetailPrice', 'DotYear',
        'WarehouseQuantities'];

    public function loadSyncProduct() {
        $enableSyncProducts = get_option('products_sync_enabled') ?? 'no';
        $latakkoCronFrequency = get_option('latakko_cron_frequency') ?? 'daily';
        $latakkoCronStartTime = get_option('latakko_cron_start_time');
        $timestamp = $latakkoCronStartTime ? strtotime($latakkoCronStartTime) : time();

        if( !wp_next_scheduled( 'syncProduct' ) && $enableSyncProducts == 'yes') {
            wp_schedule_event( $timestamp, $latakkoCronFrequency, 'syncProduct' );
        }
    }
    public static function syncProduct() {
        $latakkoWoo = new LatakkoWoo();
        update_option('start_number', 0);
        $latakkoWoo->getProductData($cronImport = true);
    }

    public static function settingCronSyncProducts() {
        try {
            $latakkoWoo = new LatakkoWoo();
            $productsSyncEnabled = $_REQUEST['productsSyncEnabled'];
            $cronStartTime = $_REQUEST['cronStartTime'];
            $cronFrequency = $_REQUEST['cronFrequency'];
            update_option('products_sync_enabled',$productsSyncEnabled);
            update_option('latakko_cron_start_time',$cronStartTime);
            update_option('latakko_cron_frequency',$cronFrequency);

            $timestamp = wp_next_scheduled ('syncProduct');

            if($timestamp) {
                wp_unschedule_event ($timestamp, 'syncProduct');
            }

            $latakkoWoo->loadSyncProduct();

            wp_send_json([
                "status"  => "OK",
                "message" => "Setting cron success"
            ]);
        } catch (Exception $exception) {
            wp_send_json([
                "status"  => "error",
                "message" => $exception->getMessage()
            ]);
        }
    }


    public function cronImport () {
        wp_clear_scheduled_hook('startImport');
        wp_schedule_single_event( time(), 'startImport');
    }

    public static function startImport() {
        $importProductProcess = new ImportProductProcess();
        $importProductProcess->task([]);
    }

    /**
     * @return void
     */
    public static function getProductData($cronImport = null)
    {
        $latakkoObject = new LatakkoWoo();
        $response = [
            "status" => "OK",
            "message" => "Get product success",
        ];

        try {
            $latakkoObject->checkApiKey();
            $accessToken = $latakkoObject->getAccessToken();
            if (!$accessToken) {
                throw new \Exception("Get product failed");
            }
            $createAt = current_time('mysql');
            $productData = $latakkoObject->getProductDataWithToken($accessToken);
            if (is_string($productData)) {
                throw new \Exception($productData);
            }
            if (!empty($productData) && is_array($productData)) {
                $latakkoObject->deleteTableProductImport();
                $data = array(
                    'product' => json_encode($productData),
                    'created_at' => $createAt,
                );
                $latakkoObject->insertToDatabase('la_product_import', $data, ['%s', '%s', '%s', '%s']);
                $totalProduct = sizeof($productData) ?? 0;
                $response['totalProductImport'] = $totalProduct;
                $latakkoObject->initImportProduct($totalProduct);
                if($cronImport) {
                    $latakkoObject->cronImport();
                }
            } else {
                throw new \Exception('Get product failed');
            }
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }
        wp_send_json($response);
    }

    /**
     * @return false|mixed|null
     * @throws Exception
     */
    public function getAccessToken() {
        $latakkoWoo = new LatakkoWoo();

        $accessToken = get_option('wgt_latakko_token') ?? null;
        $dateExpires = get_option('wgt_latakko_token_date_expires') ?? null;

        if(!$accessToken || !$dateExpires) {
            $accessToken = $latakkoWoo->getTokenFromApi();

        } elseif ($accessToken && $dateExpires) {
            $currentDate = new DateTime("now", $dateExpires->getTimezone());
            if($currentDate > $dateExpires) {
                $accessToken = $latakkoWoo->getTokenFromApi();
            }
        }

        return $accessToken;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getTokenFromApi()
    {
        $apiUser = get_option('latakko_api_user');
        $apiUserPassword = get_option('latakko_api_pass');

        if (!$apiUser || !$apiUserPassword) {
            update_option('latakko_import_product_status', 'fail');

            throw new Exception('API UserName or Rest Api is invalid');
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => GET_TOKEN_API,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=password&username='.$apiUser.'&password='.$apiUserPassword,
            CURLOPT_HTTPHEADER => array(CONTENT_TYPE),
        ));

        $tokenData = json_decode(curl_exec($curl));

        $accessToken = $tokenData->access_token ?? null;
        $dateExpires = $tokenData->{'.expires'} ?? null;

        if(!$accessToken) {
            throw new Exception('An error occurred while retrieving product data');
        }

        if($dateExpires) {
            $dateExpires = new DateTime($tokenData->{'.expires'});
            update_option('wgt_latakko_token_date_expires', $dateExpires);
        }

        update_option('wgt_latakko_token', $accessToken);

        return $accessToken;
    }

    /**
     * @param $tableName
     * @param $data
     * @param $format
     * @return bool|int|mysqli_result|resource|null
     */
    private function insertToDatabase($tableName, $data, $format)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $tableName;
        return $wpdb->insert($table_name, $data, $format);
    }


    /**
     * @return void
     */
    public static function cancelImportProduct() {
        $importProductProcess = new ImportProductProcess;
        $latakkoWoo = new LatakkoWoo();
        add_option('wgt_cancel_import', 'true');
        update_option(\ImportProductProcess::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_CANCEL);
        $latakkoWoo->initImportProduct(0);
        $importProductProcess->cancelProcess();
        wp_send_json([
            "status"  => "OK",
            "message" => "cancel",
        ]);
    }

    /**
     * @return void
     */
    public function deleteTableProductImport() {
        global $wpdb;
        $table  = $wpdb->prefix.'la_product_import';
        $wpdb->query(  $wpdb->prepare("DELETE FROM $table"));
    }

    /**
     * @return void
     */
    public static function importProductData()
    {
        update_option('wgt_cancel_import', 'false');
        $response = [
            'status' => 'OK',
            "message" => 'Importing...',
        ];
        try {
            $latakkoWoo = new LatakkoWoo();
            $productList = $latakkoWoo->getProductList();
            if (empty($productList)) {
                throw new \Exception('No products to import');
            }
            update_option(\ImportProductProcess::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_IN_PROCESS);
            update_option(\ImportProductProcess::TOTAL_PRODUCT_NEED_IMPORT, count($productList));
            update_option('start_number', 0);
            $latakkoWoo->cronImport();
        } catch (Exception $exception) {
            update_option(\ImportProductProcess::IMPORT_PRODUCT_STATUS, \ImportProductProcess::STATUS_FAIL);
            $response = [
                'status' => 'error',
                "message" => $exception->getMessage(),
            ];
        }

        wp_send_json($response);
    }

    public static function continuteImportProduct () {
        $latakkoWoo = new LatakkoWoo();
        $latakkoWoo->cronImport();
    }


    /**
     * @param $product
     * @param $woocommerce
     * @param $categoryId
     * @return void
     */
    public function importProduct($product, $category = []) {
        $latakko = new LatakkoWoo();

        $productSku = sanitize_title($product->ArticleText);
        $productCreated = get_page_by_path( $productSku, OBJECT, 'product' );
        if(!$productCreated) {
            $new_product = array(
                'post_title' => $product->ArticleText,
                'post_name' => $productSku,
                'post_status' => 'publish',
                'post_type' => 'product',
                'meta_input' => array(
                    '_sku' => $productSku,
                )
            );

            $post_id = wp_insert_post( $new_product );
            if($post_id) {
                $latakko->updateDataProduct($post_id, $product, $category);
            }
        } else {
            $latakko->updateDataProduct($productCreated->ID, $product, $category);
        }

    }

    public function updateDataProduct($postId, $product, $category = []) {
        $imageId = '';
        $quantityAvailable = $product->QuantityAvailable ?? 0;
        update_post_meta( $postId, '_regular_price', $product->Price ? (string)$product->Price : '' );
        update_post_meta( $postId, '_price', $product->Price ? (string)$product->Price : '' );
        update_post_meta( $postId, '_manage_stock', 'yes' );
        update_post_meta( $postId, '_stock', $quantityAvailable );
        update_post_meta( $postId, '_stock_status', $quantityAvailable > 0 ? 'instock' : 'outofstock'  );
        update_post_meta( $postId, '_backorders', 'no' );
        update_post_meta( $postId, '_weight',  $product->Weight ? (string)$product->Weight : '');
        update_post_meta( $postId, '_length', '' );
        update_post_meta( $postId, '_width', $product->Width ? (string)$product->Width : '' );
        update_post_meta( $postId, '_height', '' );
        $this->updateAttributeForProduct($postId, $product);

        if(!empty($category)) {
            wp_set_object_terms( $postId, array( $category->slug ), 'product_cat' );
        }

        if($product->ImageId) {
            $imageId  = LatakkoWoo::getImagesById($product->ImageId);
        }

        if($imageId) {
            set_post_thumbnail($postId , $imageId );
        }

    }
    public function updateAttributeForProduct($postId, $product) {
        $latakko = new LatakkoWoo();
        $product_attributes = [];
        foreach ($latakko->attributeCreationList as $attribute) {
            if($product->{$attribute} === false) {
                $product->{$attribute} = 'false';
            } elseif ($product->{$attribute} === true) {
                $product->{$attribute} = 'true';
            }

            if(!empty($product->{$attribute}) && wc_attribute_taxonomy_id_by_name($attribute)) {
                $product_attributes[sanitize_title($attribute)] = [
                    'name' => $attribute,
                    'value' => $product->{$attribute},
                    'position' => 0,
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 0
                ];
            }
        }

        if(!empty($product_attributes)) {
            update_post_meta( $postId, '_product_attributes', $product_attributes );
        }
    }

    public static function getAmountProductEntered() {
        $amountProductEntered = get_option(\ImportProductProcess::TOTAL_PRODUCT_SUCCEED);
        $importProductStatus = get_option(\ImportProductProcess::IMPORT_PRODUCT_STATUS);
        $amountProductFail = get_option(\ImportProductProcess::TOTAL_PRODUCT_FAILED);
        $productName = get_option(\ImportProductProcess::IMPORT_PRODUCT_NAME, null);

        wp_send_json([
            'status'  => 'OK',
            'amountProductEntered' => $amountProductEntered,
            'amountProductFail' => $amountProductFail,
            'importProductStatus' => $importProductStatus,
            'productName' => $productName,
        ]);

    }

    /**
     * @param $id
     * @return int|WP_Error|null
     */
    public function getImagesById($id = null) {
        $userToken = $this->getAccessToken();
        $attachId = null;
        if ($id && $userToken) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => GET_IMAGE_API . '?id=' . $id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '. $userToken,
                ),
            ));

            $img =curl_exec($curl);
        }

        if($img) {
            $attachId = LatakkoWoo::saveImages(base64_encode($img), $id);
        }

        return $attachId;
    }

    /**
     * @param $img
     * @param $title
     * @return int|WP_Error
     */
    function saveImages($img,$imgId )
    {
        $uploadDir = wp_upload_dir();
        $uploadPath = str_replace('/', DIRECTORY_SEPARATOR, $uploadDir['path']) . DIRECTORY_SEPARATOR;

        $img = str_replace(' ', '+', $img);
        $decoded = base64_decode($img);
        $hashedFileName = 'product-image-'. $imgId . '.jpeg';
        $isExist = $this->does_file_exists($hashedFileName) ?? null;

        if($isExist) {
            $attachId = $isExist;
        } else {
            $fileType = 'image/jpeg';

            $uploadFile = file_put_contents($uploadPath . $hashedFileName, $decoded);

            $attachment = array(
                'post_mime_type' => $fileType,
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($hashedFileName)),
                'post_content' => '',
                'post_status' => 'inherit',
                'guid' => $uploadDir['url'] . '/' . basename($hashedFileName)
            );

            $attachId = wp_insert_attachment($attachment, $uploadDir['path'] . '/' . $hashedFileName);

            require_once(ABSPATH . 'wp-admin/includes/image.php');

            $attach_data = wp_generate_attachment_metadata($attachId, $uploadDir['path'] . '/' . $hashedFileName);
            wp_update_attachment_metadata($attachId, $attach_data);
        }

        return $attachId;
    }

    function does_file_exists($filename) {
        global $wpdb;

        return intval( $wpdb->get_var( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_wp_attached_file'
                            AND meta_value LIKE '%/$filename'" ) );
    }

    /**
     * @param $token
     * @return mixed
     */
    public function getProductDataWithToken($token) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => GET_PRODUCT_API,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array('Authorization: Bearer'.' '.$token),
        ));

        $productData = json_decode(curl_exec($curl));

        return $productData;
    }

    /**
     * @param $product
     * @param $woocommerce
     * @return array|object|stdClass[]|void|null
     */
    public function addCategory($product) {
        $latakkoWoo = new LatakkoWoo();
        $parentId = 0;
        if($product->MainGroupName) {
            $categoryName = explode('/', $product->MainGroupName);
            foreach ($categoryName as $name) {
                $categorySlug = sanitize_title($name);
                $category = get_term_by('slug', $categorySlug, 'product_cat');
                if ($category) {
                    $parentId = $category->term_id;
                } else {
                    $categoryCreate = $latakkoWoo->createCategoryWooCom($name, $parentId) ?? null;
                    if(!empty($categoryCreate)) {
                        $parentId = $categoryCreate['term_id'];
                    }
                }
            }

            $slugLastCategory = sanitize_title($categoryName[array_key_last($categoryName)]);

            return  get_term_by('slug', $slugLastCategory, 'product_cat');
        }

    }

    /**
     * @param $name
     * @param $parentId
     * @return void
     */
    public function createCategoryWooCom($name, $parentId = 0) {
        $category = wp_insert_term(
            $name,
            'product_cat',
            array(
                'slug' => sanitize_title($name),
                'parent' => $parentId
            )
        );

        return $category;
    }

    /**
     * @param $sku
     * @return WC_Product|null
     */
    public function getProductBySku($sku) {
        global $wpdb;
        $table = $wpdb->prefix.'postmeta';
        $product = null;
        $product_id = $wpdb->get_var($wpdb->prepare(
            "SELECT post_id FROM $table WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku));

        if ( $product_id ) {
            $product = new WC_Product( $product_id );
        }

        return $product;
    }

    /**
     * @param $product
     * @return void
     */
    public function saveAttributeOfProduct($product) {
        $latakko = new LatakkoWoo();
        $attributeCreationList = $latakko->attributeCreationList;
        foreach ($product as $name => $attributeValue) {
            if (in_array($name, $attributeCreationList)) {
                if (!wc_attribute_taxonomy_id_by_name($name) ) {
                    $latakko->createAttribute($name, sanitize_title($name));
                }

                if ($attributeValue === false) {
                    $attributeValue = "false";
                } elseif ($attributeValue === true) {
                    $attributeValue = "true";
                }

                if($attributeValue || $attributeValue === false || $attributeValue === true) {
                    if(!empty(wc_attribute_taxonomy_id_by_name($name))) {
                        $latakko->createTerm($attributeValue, sanitize_title($attributeValue), $name);
                    }
                }
            }
        }
    }

    public function createAttribute(string $attributeName, string $attributeSlug) {
        delete_transient('wc_attribute_taxonomies');
        \WC_Cache_Helper::incr_cache_prefix('woocommerce-attributes');

        $attributeLabels = wp_list_pluck(wc_get_attribute_taxonomies(), 'attribute_label', 'attribute_name');
        $attributeWCName = array_search($attributeSlug, $attributeLabels, TRUE);

        if (! $attributeWCName) {
            $attributeWCName = wc_sanitize_taxonomy_name($attributeSlug);
        }

        $attributeId = wc_attribute_taxonomy_id_by_name($attributeWCName);
        if (! $attributeId) {
            $taxonomyName = wc_attribute_taxonomy_name($attributeWCName);
            unregister_taxonomy($taxonomyName);
            wc_create_attribute(array(
                'name' => $attributeName,
                'slug' => $attributeSlug,
                'type' => 'select',
                'order_by' => 'menu_order',
                'has_archives' => 0,
            ));

            register_taxonomy($taxonomyName, apply_filters('woocommerce_taxonomy_objects_' . $taxonomyName, array(
                'product'
            )), apply_filters('woocommerce_taxonomy_args_' . $taxonomyName, array(
                'labels' => array(
                    'name' => $attributeSlug,
                ),
                'hierarchical' => FALSE,
                'show_ui' => FALSE,
                'query_var' => TRUE,
                'rewrite' => FALSE,
            )));
        }
    }


    public function createTerm(string $termName, string $termSlug, string $taxonomy) {
        $taxonomy = wc_attribute_taxonomy_name($taxonomy);

        if (! get_term_by('slug', $termSlug, $taxonomy)) {
            wp_insert_term($termName, $taxonomy, array(
                'slug' => $termSlug,
            ));
        }
    }

    /**
     * @param $sku
     * @return bool
     */
    public function productIsExist($sku) {
        $result = false;
        $latakko = new LatakkoWoo();

        if ($latakko->getProductBySku($sku)) {
            $result = true;
        }

        return $result;
    }

    /**
     * @return array|object|stdClass|null
     */
    public function getProductList() {
        global $wpdb;
        $limit = 1;
        $offset = 0;
        $table = $wpdb->prefix . 'la_product_import';
        $sql = "SELECT * FROM {$table} ORDER BY id DESC  LIMIT %d OFFSET %d";
        $productList = $wpdb->get_row( $wpdb->prepare($sql, $limit, $offset), ARRAY_A) ?? null;

        if(!empty($productList) && !empty($productList['product'])) {
            $productList = $productList['product'];
        }

        return json_decode($productList);
    }

    public static function optionsPage()
    {
        add_menu_page(
            'Latakko Settings',
            'Latakko Settings',
            'manage_options',
            'latakko-settings',
            'LatakkoWoo::options_page_html'
        );
    }

    public static function registerSettings()
    {
        register_setting('latakko_options', 'latakko_api_user');
        register_setting('latakko_options', 'latakko_api_pass');
        register_setting('latakko_token_options', 'wgt_latakko_token');
        register_setting('latakko_token_options', 'wgt_latakko_token_date_expires');
        register_setting('latakko_options', 'wgt_product_name_importing');
        register_setting('latakko_options', \ImportProductProcess::TOTAL_PRODUCT_SUCCEED);
        register_setting('latakko_options', \ImportProductProcess::TOTAL_PRODUCT_FAILED);
        register_setting('latakko_options', \ImportProductProcess::IMPORT_PRODUCT_STATUS);
        register_setting('latakko_options', \ImportProductProcess::TOTAL_PRODUCT_NEED_IMPORT);
    }

    public static function load_async_class()
    {
        new LatakkoAsync();
        new LatakkoTruckAsync();
        new ImportProductProcess();
    }

    /**
     * @throws Exception
     */
    private function checkApiKey()
    {
        $apiUser = get_option('latakko_api_user');
        $apiUserPassword = get_option('latakko_api_pass');

        if (!$apiUser || !$apiUserPassword) {
            throw new \Exception("API UserName or Rest Api is invalid");
        }

        return true;
    }

    private function initImportProduct($totalProduct)
    {
        update_option(\ImportProductProcess::TOTAL_PRODUCT_SUCCEED, 0);
        update_option(\ImportProductProcess::TOTAL_PRODUCT_FAILED, 0);
        update_option(\ImportProductProcess::TOTAL_PRODUCT_NEED_IMPORT, $totalProduct);
        update_option('wgt_product_name_importing', '');
    }

    public static function options_page_html()
    {
        if (!current_user_can('manage_options')) {
            return;
        }
        ?>
        <div id="loading"></div>
        <div id="latakko_app" class="wrap">
            <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
            <form action="options.php" method="post">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">API UserName</th>
                        <td>
                            <input type="text" id="latakko_api_user" name="latakko_api_user" size="50"
                                   value="<?php echo esc_attr(get_option('latakko_api_user')); ?>"/>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Rest Api</th>
                        <td>
                            <input type="text" id="latakko_api_pass" name="latakko_api_pass" size="50"
                                   value="<?php echo esc_attr(get_option('latakko_api_pass')); ?>"/>
                        </td>
                    </tr>
                </table>
                <?php
                settings_fields('latakko_options');
                submit_button('Save');
                ?>
            </form>
            <?php
            $importProductStatus = get_option(\ImportProductProcess::IMPORT_PRODUCT_STATUS);
            $totalProductImport = get_option(\ImportProductProcess::TOTAL_PRODUCT_NEED_IMPORT);
            $amountProductEntered = get_option(\ImportProductProcess::TOTAL_PRODUCT_SUCCEED);
            $amountProductFail = get_option(\ImportProductProcess::TOTAL_PRODUCT_FAILED);
            $importProductStatus = !empty($importProductStatus) ? $importProductStatus : '';
            $totalProductImport = !empty($totalProductImport) ? $totalProductImport : 0;
            $amountProductEntered = !empty($amountProductEntered) ? $amountProductEntered : 0;
            $amountProductFail = !empty($amountProductFail) ? $amountProductFail : 0;
            $productsSyncEnabled = get_option('products_sync_enabled') ?? 'false';
            $latakkoCronFrequency = get_option('latakko_cron_frequency') ?? 'daily';
            $enabledGroup = ['yes' => 'Yes', 'no' => 'No'];
            $frequencyGroup = ['daily' => 'Daily',
                'weekly'=> 'Weekly',
                'monthly' => 'Monthly']

            ?>
            <div id="option_cron" class="overlay">
                <div class="popup">
                    <a id="close" class="close" href="#">&times;</a>
                    <div class="content">
                        <div class="cron-field">
                            <div class="cron-label">
                                <label class="wgt-label" for="products_sync_enabled">Enable Sync Products</label>
                            </div>
                            <select id="products_sync_enabled" name="products_sync_enabled">
                                <?php foreach ($enabledGroup as $key => $value): ?>
                                    <option value="<?php echo esc_attr($key) ?>"
                                        <?php echo esc_attr($key == $productsSyncEnabled ? 'selected' : '') ?>>
                                        <?php echo esc_attr($value) ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="cron-field">
                            <div class="cron-label">
                                <label class="wgt-label" for="latakko_cron_start_time">Start time (UTC)</label>
                            </div>
                            <input id="latakko_cron_start_time"
                                   value="<?php echo esc_attr(get_option('latakko_cron_start_time')); ?>"
                                   type="time" name="latakko_cron_start_time" step="2" />
                        </div>

                        <div class="cron-field">
                            <div class="cron-label">
                                <label class="wgt-label" for="latakko_cron_frequency">Frequency</label>
                            </div>
                            <select id="latakko_cron_frequency" name="latakko_cron_frequency">
                                <?php foreach ($frequencyGroup as $key => $value): ?>
                                    <option value="<?php echo esc_attr($key) ?>"
                                        <?php echo esc_attr($key == $latakkoCronFrequency ? 'selected' : '') ?>>
                                        <?php echo esc_attr($value) ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <button @click="settingCron" class="wgt-import-product-submit">Submit</button>
                    </div>
                </div>
            </div>

            <div id="wgt-message-processing" class="mini-toastr">
                <div class="mini-toastr__notification -success">
                    <div id="mini-toastr-notification__message" class="mini-toastr-notification__message">Getting
                        products...
                    </div>
                </div>
            </div>
            <input id="import_value" type="hidden" value=""/>
            <div>
                <a href="#option_cron">
                    <button @click="showOptionCron" class="button button-primary">Setting Cron</button>
                </a>
                <?php if (!empty($importProductStatus) && !in_array($importProductStatus, [\ImportProductProcess::STATUS_COMPLETE, \ImportProductProcess::STATUS_CANCEL])): ?>
                    <button @click="continuteImport" id="wgt_btn_continute" class="button button-primary">Continue</button>
                    <button @click="cancelImport" id="wgt_btn_cancel" class="button button-primary">Cancel</button>
                    <div id="product-import-process" class="product-import-process">
                        <div class="product-status-label">
                            <span>Import status:</span><br>
                            <span class="import-label">Total product:</span><br>
                            <span class="import-label">Amount product entered:</span><br>
                            <span class="import-label">Amount product fail:</span>
                        </div>
                        <div class="product-status-value">
                            <span id="import-product-status"><?php echo esc_attr($importProductStatus); ?></span><br>
                            <span id="total-product"><?php echo esc_attr($totalProductImport); ?></span><br>
                            <span id="amount-product-entered"><?php echo esc_attr($amountProductEntered); ?></span><br>
                            <span id="amount-product-fail"><?php echo esc_attr($amountProductFail); ?></span>

                        </div>
                        <div>
                            <span id="wgt-product-name"></span>
                        </div>
                        <input type="hidden" id="import-product-status-value"
                               value="<?php echo esc_attr($importProductStatus); ?>">
                        <input type="hidden" id="import-product-total-value"
                               value="<?php echo esc_attr($totalProductImport); ?>">
                    </div>
                <?php else: ?>
                    <button @click="getProductData" id="get_product_data" class="button button-primary"
                            <?php if ($importProductStatus == \ImportProductProcess::STATUS_PENDING): ?>disabled<?php endif; ?>>
                        Start Import Product
                    </button>
                <?php endif; ?>
            </div>
        </div>
        <?php
    }
}